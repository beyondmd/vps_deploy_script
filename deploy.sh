#!/bin/sh

# relative path fix
cd "${0%"/"*}"
[ "$USER" != "root" ] && echo "Script needs to be run with root privliges" && exit

USERVAR=$(cat .user)
[ -z $USERVAR ] && read -p "Type which user to give doas/sudo privliges to (leave blank to not give privilages): " USERVAR
echo "* 4 * * * $PWD/pull.sh"

#default directories
mkdir ../apps ../bots ../other
echo "created default folders in home directory"

#remove sudo
apt remove -y sudo

#misc installs
apt install -y stow build-essential git bison wget curl

#install/update caddy
apt remove caddy
apt install -y debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | tee /etc/apt/trusted.gpg.d/caddy-stable.asc
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | tee /etc/apt/sources.list.d/caddy-stable.list
apt update
apt install caddy

#install/update docker
apt remove docker docker-engine docker.io containerd runc
apt install -y ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update
apt install -y docker-ce docker-ce-cli containerd.io
usermod -aG docker $USERVAR

#install docker-compose
rm -rf /usr/local/bin/docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

#root bashrc
rm -rf /root/.bashrc
cp ./dotfiles/.rootbashrc /root/.bashrc

chown -R $USERVAR .

cd dotfiles
rm /home/$USERVAR/.bashrc
stow -t /home/$USERVAR .
cd ..

#doas install
mkdir ../.source
git clone https://github.com/Duncaen/OpenDoas.git ../.source
cd ../.source
./configure --with-timestamp
make clean install
echo permit persist keepenv $USERVAR as root >> /etc/doas.conf

#update script nightly
# add cronjob if it doesnt exist
crontab -l | grep -q
[ $? != 0 ] && (crontab -l 2>/dev/null; echo "* 4 * * * $PWD/pull.sh") | crontab -
