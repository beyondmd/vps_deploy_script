#!/bin/sh
cd "${0%"/"*}"
[ "$USER" != "root" ] && echo "Script needs to be run with root privliges" && exit

USERISCOR(){
  read -p "Is username \"$USERVAR\" correct? (lowercase y/n) " USERCOR
}
USERLOOP(){
  if [ "$USERCOR" != "y" ]; then
    read -p "Username (leave blank for \"admin\"): " USERVAR
    [ -z $USERVAR ] && USERVAR=admin
    USERISCOR
    USERLOOP
  fi
}

HOSTNAMEISCOR(){
  read -p "Is hostname \"$HOSTNAMEVAR\" correct? (lowercase y/n) " HOSTNAMECOR
}
HOSTNAMELOOP(){
  if [ "$HOSTNAMECOR" != "y" ]; then
    read -p "Hostname (leave blank for \"changeme\"): " HOSTNAMEVAR
    [ -z $HOSTNAMEVAR ] && HOSTNAMEVAR=changeme && echo "You can change the hostname later w/ \"sudo hostnamectl set-hostname \$hostname\""
    HOSTNAMEISCOR
    HOSTNAMELOOP
  fi
}

USERLOOP
adduser $USERVAR
echo $USERVAR > .user

HOSTNAMELOOP
hostnamectl set-hostname $HOSTNAMEVAR

mv $PWD /home/$USERVAR/
echo "Current folder moved \"/home/$USERVAR/\", please move to it and run \"./deploy.sh\""
